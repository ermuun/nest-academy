import React from 'react';
import { Border, Button, Text, Box, Spacing } from '../../components';
import { CheckBoxItem, GroupCheckBox } from '../../components/group-checkbox';


export const ExamCardAnswer: React.FC<any> = (props) => {
  const ans = ['A', 'B', 'C', 'D'];
  const { funct, type, index, answer } = props;
  console.log(type);
  console.log(index);
  console.log(answer);
  return (
    <CheckBoxItem checkbox index={0}>
      <Border radius={5}>
        <Button
          category={'ghost'}
          type={'primary'}
          width={56}
          height={56}
          onPress={() => {
            type == 'graphicquizwithimage' ? funct(ans[index]) : funct(answer);
          }}
        >
          <Text bold>{ans[index]}</Text>
        </Button>
      </Border>
    </CheckBoxItem>
  );
};
