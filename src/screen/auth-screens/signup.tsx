import React, { useContext, useState } from 'react';
import { ProgressSteps, ProgressStep, Box } from '../../components';
import { AuthContext } from '../../provider/auth';
import { Auth1, Auth2, Auth3 } from './auths/index';
import { useCollection } from '../../hooks';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const Signup = () => {
  const navigation = useNavigation();
  const { user, signInWithPhoneNumber, confirmCode } = useContext(AuthContext);
  const { updateRecord: createUser } = useCollection('users');
  const [state, setState] = useState({ firstname: '', lastname: '' });
  const section1 = async () => await signInWithPhoneNumber();
  const section2 = async () => await confirmCode();
  const section4 = async () => {
    await createUser(user && user.uid, state);
    await navigation.navigate(NavigationRoutes.Success, {
      successMessage:
        'Таны бүртгэл амжилттай үүслээ. Та манай аппликэшнд тавтай морил.',
    });
  };

  return (
    <Box flex={1} role={'white'}>
      <ProgressSteps lastButtonFunction={section4}>
        <ProgressStep label="Auth1" id="Auth1" onNext={section1}>
          <Auth1 title=" Гар утасны дугаараа оруулна уу" />
        </ProgressStep>
        <ProgressStep label="Auth2" id="Auth2" onNext={section2}>
          <Auth2 />
        </ProgressStep>
        <ProgressStep label="Auth3" id="Auth3">
          <Auth3 setState={setState} />
        </ProgressStep>
      </ProgressSteps>
    </Box>
  );
};
