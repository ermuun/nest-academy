import React from 'react';
import { SafeAreaView } from 'react-native';
import { Box, Text, DigitInput, Spacing } from '../../../components';

export const Auth2 = () => {
  return (
    <Box flex={1} role={'white'}>
      <Spacing mt={5}>
        <Box height={44} justifyContent="center" alignItems="center">
          <Text height={44} bold textAlign="center" width={269} type="headline">
            Таны дугаарт илгээсэн
            6 оронтой кодыг оруулна уу
          </Text>
        </Box>
      </Spacing>
      <Spacing mt={12} mb={22}>
        <Box height={56} justifyContent="center" alignItems="center">
          <DigitInput />
        </Box>
      </Spacing>
    </Box>
  );
};
