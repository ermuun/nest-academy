import React, { useState } from 'react';
import {
  TouchableOpacity,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import { Banner, Border, Button, Text } from '../../components';
import { Box, Queue, Spacing, Stack } from '../../components/layout';

export const ScheduleMeetingTimeScreen = (props: any) => {
  const [width1, setWidth1] = useState('100%');
  const [width2, setWidth2] = useState('100%');
  const [width3, setWidth3] = useState('100%');
  const [width4, setWidth4] = useState('100%');
  const [visible, setVisible] = useState(0);
  const [visible1, setVisible1] = useState(0);
  const [visible2, setVisible2] = useState(0);
  const [visible3, setVisible3] = useState(0);
  const month = props.route.params.pickedDay.month;
  const day = props.route.params.pickedDay.day;
  const spellMonth =
    month === 1
      ? 'Jan'
      : month === 2
      ? 'Feb'
      : month === 2
      ? 'Feb'
      : month === 3
      ? 'Mar'
      : month === 4
      ? 'Apr'
      : month === 5
      ? 'May'
      : month === 6
      ? 'June'
      : month === 7
      ? 'July'
      : month === 8
      ? 'Aug'
      : month === 9
      ? 'Sep'
      : month === 10
      ? 'Oct'
      : month === 11
      ? 'Nov'
      : 'Dec';
  console.log(props.route.params.pickedDay);
  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  return (
    <Box>
      <Spacing ml={4} mr={4} mt={12} mb={4}>
        <Stack size={4}>
          <Spacing mb={2}>
            <Box width={'100%'} height={129}>
              <Banner type={'info'} title={'Мэдэгдэл'}>
                <Text numberOfLines={3} type={'callout'}>
                  Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!
                </Text>
              </Banner>
            </Box>
          </Spacing>

          <Text
            role={'primary500'}
            bold
            type={'headline'}
            fontFamily={'Montserrat'}
          >
            Та боломжит цагаа сонгоно уу
          </Text>
        </Stack>
      </Spacing>
      <Box
        justifyContent={'center'}
        width={'100%'}
        height={54}
        role={'primary300'}
      >
        <Spacing p={4}>
          <Text fontFamily={'Montserrat'} bold type={'headline'}>
            {spellMonth} {day}
          </Text>
        </Spacing>
      </Box>
      <Box>
        <Spacing pt={4} pr={3.5} pl={3.5}>
          <Stack size={3}>
            <Queue justifyContent={'space-between'}>
              <Box width={width1} height={42}>
                <Border grow={1} radius={4} lineWidth={1} role={'primary300'}>
                  <TouchableOpacity
                    style={{ backgroundColor: 'black', width: '100%' }}
                    onPress={() => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.linear
                      ),
                        setWidth1(width1 === '100%' ? '47%' : '100%'),
                        setVisible(visible === 1 ? 0 : 1);
                    }}
                  >
                    <Box
                      width={'100%'}
                      height={'100%'}
                      role={'primary200'}
                      justifyContent={'center'}
                      alignItems={'center'}
                    >
                      <Text bold fontFamily={'Montserrat'} type={'subheading'}>
                        9:00 PM
                      </Text>
                    </Box>
                  </TouchableOpacity>
                </Border>
              </Box>
              <Box height={42} width={'47%'} opacity={visible}>
                <Button width={'100%'} onPress={() => console.log('go')}>
                  <Text
                    bold
                    fontFamily={'Montserrat'}
                    type={'subheading'}
                    role={'white'}
                  >
                    Сонгох
                  </Text>
                </Button>
              </Box>
            </Queue>

            <Queue justifyContent={'space-between'}>
              <Box width={width2} height={42}>
                <Border grow={1} radius={4} lineWidth={1} role={'primary300'}>
                  <TouchableOpacity
                    style={{ backgroundColor: 'black', width: '100%' }}
                    onPress={() => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.linear
                      ),
                        setWidth2(width2 === '100%' ? '47%' : '100%'),
                        setVisible1(visible1 === 1 ? 0 : 1);
                    }}
                  >
                    <Box
                      width={'100%'}
                      height={'100%'}
                      role={'primary200'}
                      justifyContent={'center'}
                      alignItems={'center'}
                    >
                      <Text bold fontFamily={'Montserrat'} type={'subheading'}>
                        12:00 PM
                      </Text>
                    </Box>
                  </TouchableOpacity>
                </Border>
              </Box>
              <Box height={42} width={'47%'} opacity={visible1}>
                <Button width={'100%'} onPress={() => console.log('go')}>
                  <Text
                    bold
                    fontFamily={'Montserrat'}
                    type={'subheading'}
                    role={'white'}
                  >
                    Сонгох
                  </Text>
                </Button>
              </Box>
            </Queue>
            <Queue justifyContent={'space-between'}>
              <Box width={width3} height={42}>
                <Border grow={1} radius={4} lineWidth={1} role={'primary300'}>
                  <TouchableOpacity
                    style={{ backgroundColor: 'black', width: '100%' }}
                    onPress={() => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.linear
                      ),
                        setWidth3(width3 === '100%' ? '47%' : '100%'),
                        setVisible2(visible2 === 1 ? 0 : 1);
                    }}
                  >
                    <Box
                      width={'100%'}
                      height={'100%'}
                      role={'primary200'}
                      justifyContent={'center'}
                      alignItems={'center'}
                    >
                      <Text bold fontFamily={'Montserrat'} type={'subheading'}>
                        13:00 PM
                      </Text>
                    </Box>
                  </TouchableOpacity>
                </Border>
              </Box>
              <Box height={42} width={'47%'} opacity={visible2}>
                <Button width={'100%'} onPress={() => console.log('go')}>
                  <Text
                    bold
                    fontFamily={'Montserrat'}
                    type={'subheading'}
                    role={'white'}
                  >
                    Сонгох
                  </Text>
                </Button>
              </Box>
            </Queue>
            <Queue justifyContent={'space-between'}>
              <Box width={width4} height={42}>
                <Border grow={1} radius={4} lineWidth={1} role={'primary300'}>
                  <TouchableOpacity
                    style={{ backgroundColor: 'black', width: '100%' }}
                    onPress={() => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.linear
                      ),
                        setWidth4(width4 === '100%' ? '47%' : '100%'),
                        setVisible3(visible3 === 1 ? 0 : 1);
                    }}
                  >
                    <Box
                      width={'100%'}
                      height={'100%'}
                      role={'primary200'}
                      justifyContent={'center'}
                      alignItems={'center'}
                    >
                      <Text bold fontFamily={'Montserrat'} type={'subheading'}>
                        14:00 PM
                      </Text>
                    </Box>
                  </TouchableOpacity>
                </Border>
              </Box>
              <Box height={42} width={'47%'} opacity={visible3}>
                <Button width={'100%'} onPress={() => console.log('go')}>
                  <Text
                    bold
                    fontFamily={'Montserrat'}
                    type={'subheading'}
                    role={'white'}
                  >
                    Сонгох
                  </Text>
                </Button>
              </Box>
            </Queue>
          </Stack>
        </Spacing>
      </Box>
    </Box>
  );
};
