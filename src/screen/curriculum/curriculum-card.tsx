import React, { useState } from 'react';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Svg, Path } from 'react-native-svg';
import { Spacing, Box, Border, Text } from '../../components';

type filter = {
  level?: string;
  lessons?: any;
  duration?: string;
  number?: string;
};

export const CurriculmCard: React.FC<filter> = ({
  level,
  lessons,
  duration,
  number,
}) => {
  const Lesson = (i) => {
    console.log(i);
    return (
      <Box flexDirection={'row'} width={'100%'}>
        <Box justifyContent={'center'} width={'10%'} alignItems={'center'}>
          <Box height={3} width={3}>
            <Border lineWidth={2} radius={2} />
          </Box>
        </Box>
        <Text type={'body'}>{i.lesson}</Text>
      </Box>
    );
  };
  return (
    <Spacing mt={4}>
      <Border radius={4}>
        <Box role={'white'} width={343} height={300}>
          {/* Title */}
          <Spacing p={4}>
            <Box>
              <Text type={'body'} fontFamily={'Montserrat'} bold>
                {level}
              </Text>
            </Box>
          </Spacing>
          <Border lineWidth={1} role={'primary200'}></Border>

          {/* Body */}
          <Box height={'63%'} justifyContent={'space-around'}>
            {lessons.map((i) => Lesson(i))}
          </Box>
          <Spacing mt={2} />
          <Border lineWidth={1} role={'primary200'}></Border>

          {/* Footer */}
          <Spacing p={3}>
            <Box
              flexDirection={'row'}
              alignItems={'center'}
              width={'85%'}
              justifyContent={'space-between'}
              alignSelf={'center'}
            >
              <Box flexDirection={'row'}>
                <Box>
                  <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <Path
                      d="M12 22C6.477 22 2 17.523 2 12C2 6.477 6.477 2 12 2C17.523 2 22 6.477 22 12C22 17.523 17.523 22 12 22ZM12 20C14.1217 20 16.1566 19.1571 17.6569 17.6569C19.1571 16.1566 20 14.1217 20 12C20 9.87827 19.1571 7.84344 17.6569 6.34315C16.1566 4.84285 14.1217 4 12 4C9.87827 4 7.84344 4.84285 6.34315 6.34315C4.84285 7.84344 4 9.87827 4 12C4 14.1217 4.84285 16.1566 6.34315 17.6569C7.84344 19.1571 9.87827 20 12 20ZM13 12H17V14H11V7H13V12Z"
                      fill="#172B4D"
                    />
                  </Svg>
                </Box>
                <Spacing ml={2}>
                  <Text type={'subheading'} bold>
                    {duration}
                  </Text>
                </Spacing>
              </Box>
              <Box flexDirection={'row'}>
                <Box>
                  <Svg width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <Path
                      d="M18 16H3C2.73478 16 2.48043 16.1054 2.29289 16.2929C2.10536 16.4804 2 16.7348 2 17C2 17.2652 2.10536 17.5196 2.29289 17.7071C2.48043 17.8946 2.73478 18 3 18H18V20H3C2.20435 20 1.44129 19.6839 0.87868 19.1213C0.316071 18.5587 0 17.7956 0 17V2C0 1.46957 0.210714 0.960859 0.585786 0.585786C0.960859 0.210714 1.46957 0 2 0H18V16ZM2 14.05C2.162 14.017 2.329 14 2.5 14H16V2H2V14.05ZM13 7H5V5H13V7Z"
                      fill="#172B4D"
                    />
                  </Svg>
                </Box>
                <Spacing ml={2}>
                  <Text type={'subheading'} bold>
                    {number}
                  </Text>
                </Spacing>
              </Box>
            </Box>
          </Spacing>
        </Box>
      </Border>
    </Spacing>
  );
};
