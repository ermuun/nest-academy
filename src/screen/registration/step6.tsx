import React, { useContext } from 'react';
import { ScrollView } from 'react-native';
import {
  Border,
  Box,
  InputWithMessage,
  ProgressStepsContext,
  Spacing,
  Stack,
  Text,
} from '../../components';

interface Props {
  check?: Object;
}

const Kid = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Spacing mh={4}>
      <Stack size={4}>
        <InputWithMessage
          width={'100%'}
          messageText={check.school && check.school.messageText}
          messageType={check.school && check.school.messageType}
          placeholder={'Сургууль'}
          value={userInfo.school}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, school: text }))
          }
        />
        <InputWithMessage
          width={'100%'}
          messageText={check.grade && check.grade.messageText}
          messageType={check.grade && check.grade.messageType}
          placeholder={'Анги'}
          value={userInfo.grade}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, grade: text }))
          }
        />
      </Stack>
    </Spacing>
  );
};
const Adult = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Spacing mh={4}>
      <Stack size={6}>
        <InputWithMessage
          width={'100%'}
          messageText={check.educationStatus && check.educationStatus.messageText}
          messageType={check.educationStatus && check.educationStatus.messageType}
          placeholder={'Боловсролын байдал'}
          value={userInfo.educationStatus}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({
              ...userInfo,
              educationStatus: text,
            }))
          }
        />
        <InputWithMessage
          width={'100%'}
          messageText={check.description && check.description.messageText}
          messageType={check.description && check.description.messageType}
          placeholder={'Эрхэлж буй ажил, сургуулийн товч танилцуулга'}
          value={userInfo.description}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, description: text }))
          }
        />
      </Stack>
    </Spacing>
  );
};
const Contact = ({ userInfo, setUserInfo, check, age }: any) => {
  const phoneFunction = (text: any) => {
    if (text.length !== 9)
      setUserInfo((userInfo: any) => ({ ...userInfo, phone: text }));
  };

  return (
    <Spacing mh={4}>
      <Stack size={4}>
        <Text type={'headline'} bold textAlign={'left'} width={343}>
          Холбоо барих
      </Text>
        <InputWithMessage
          width={'100%'}
          messageText={check.email && check.email.messageText}
          messageType={check.email && check.email.messageType}
          placeholder={'И-мейл'}
          value={userInfo.email}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, email: text }))
          }
          keyboardType={'email-address'}
        />
        <InputWithMessage
          width={'100%'}
          messageText={check.phone && check.phone.messageText}
          messageType={check.phone && check.phone.messageType}
          placeholder={'Яааралтай үед холбоо барих'}
          value={userInfo.phone}
          onChangeText={(text: string) => phoneFunction(text)}
          keyboardType={'number-pad'}
        />
        {age < 18 ? (
          <></>
        ) : (
            <InputWithMessage
              width={'100%'}
              messageText={check.facebook && check.facebook.messageText}
              messageType={check.facebook && check.facebook.messageType}
              placeholder={'Таны Facebook хаяг'}
              value={userInfo.facebook}
              onChangeText={(text: string) =>
                setUserInfo((userInfo: any) => ({ ...userInfo, facebook: text }))
              }
            />
          )}
      </Stack>
    </Spacing>
  );
};
const FromWho = ({ userInfo, setUserInfo, check }: any) => {
  return (
    <Spacing mh={4}>
      <Stack size={4}>
        <Text type={'headline'} bold textAlign={'left'} width={343}>
          Хаанаас олж мэдсэн бэ?
      </Text>
        <InputWithMessage
          width={'100%'}
          messageText={check.fromWho && check.fromWho.messageText}
          messageType={check.fromWho && check.fromWho.messageType}
          placeholder={'Facebook'}
          value={userInfo.fromWho}
          onChangeText={(text: string) =>
            setUserInfo((userInfo: any) => ({ ...userInfo, fromWho: text }))
          }
        />
      </Stack>
    </Spacing>
  );
};

export const Step6: React.FC<Props> = ({ check }) => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);
  const currentYear = new Date().getFullYear();
  const age = currentYear - Number(userInfo.date.slice(0, 4));

  return (
    <Box flex={1}>
      <ScrollView
        contentContainerStyle={{ alignItems: 'center' }}
        showsVerticalScrollIndicator={false}
      >
        <Spacing mv={7}>
          <Stack size={6}>
            <Stack size={2} alignItems={'flex-start'}>
              <Spacing mh={4}>
              <Text type={'headline'} bold textAlign={'left'} width={343}>
                Элсэгчийн мэдээлэл
              </Text>
              <Text
                width={300}
                role={'primary400'}
                type={'body'}
                textAlign={'left'}
              >
                Та бидэнд үнэн зөв мэдээлэл өгсөнөөр бүртгэл амжилттай дуусна
              </Text>
              </Spacing>
            </Stack>

            {age < 18 ? (
              <Kid check={check} userInfo={userInfo} setUserInfo={setUserInfo} />
            ) : (
                <Adult
                  check={check}
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                />
              )}
          </Stack>

          <Spacing mb={5} mt={5}>
            <Border radius={3} lineWidth={1} role={'offwhite'} />
          </Spacing>

          <Contact
            age={age}
            check={check}
            userInfo={userInfo}
            setUserInfo={setUserInfo}
          />

          <Spacing mv={5}>
            <Border radius={3} lineWidth={1} role={'offwhite'} />
          </Spacing>

          <FromWho
            check={check}
            userInfo={userInfo}
            setUserInfo={setUserInfo}
          />
        </Spacing>
      </ScrollView>
    </Box >
  );
};
