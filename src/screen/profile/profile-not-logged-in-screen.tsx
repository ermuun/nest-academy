import React, { useContext, useEffect } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { Button } from '../../components/core';
import { Box, Text, Stack, Spacing } from '../../components';
import {
  Phone,
  Circle,
  CircleShadow,
  Rectangle,
  XIcon,
  Triangle,
  TriangleShadow,
} from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { AuthContext } from '../../provider/auth';

export const ProfileNotLoggedInScreen = () => {
  const { user } = useContext(AuthContext);
  const navigation = useNavigation();

  // useEffect(() => {
  //   if (user) {
  //     navigation.navigate(NavigationRoutes.MainRoot);
  //   }
  // }, []);

  return (
    <SafeAreaView style={{ backgroundColor: '#F9F9FA' }}>
      <Box display="flex" height="100%" alignItems={'center'}>
        <Box position="absolute" top={20} right={20}>
          <TouchableOpacity onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}>
            <XIcon role={'primary500'} width={32} height={32} />
          </TouchableOpacity>
        </Box>
        <Box flex={1}></Box>
        <Box flex={2}>
          <Spacing mh={15}>
            <Box justifyContent="flex-start">
              <Stack size={3}>
                <Spacing mh={10}>
                  <Text
                    type={'title3'}
                    role={'primary500'}
                    textAlign={'center'}
                    bold
                    numberOfLines={2}
                  >
                    Та одоогоор бүртгэлгүй байна
                  </Text>
                </Spacing>
                <Text
                  type={'body'}
                  role={'primary500'}
                  textAlign={'center'}
                  numberOfLines={3}
                >
                  Та бүртгэлсэнээр үнэгүй хичээл үзэх болон элсэлтэд бүртгүүлэх
                  боломжтүй болно
                </Text>
                <Box alignSelf="center" width={151}>
                  <Button
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.WelcomeScreen)
                    }
                    size={'l'}
                    width={151}
                  >
                    <Text type={'callout'} role={'white'} bold>
                      Бүртгүүлэх
                    </Text>
                  </Button>
                </Box>
              </Stack>
            </Box>
          </Spacing>
        </Box>
        <Box flex={5}></Box>
          <Box position={'absolute'} bottom={0}>
            <Phone height={375} width={225} role="primary100" />
          </Box>
          <Box position="absolute" bottom={0} right={0}>
            <Rectangle />
          </Box>
          <Box position="absolute" bottom={100} left={0}>
            <Circle />
          </Box>
          <Box position="absolute" bottom={100} left={0}>
            <CircleShadow />
          </Box>
          <Box position="absolute" bottom={0} left={45}>
            <Triangle />
          </Box>
          <Box position="absolute" bottom={0} left={0}>
            <TriangleShadow />
          </Box>
      </Box>
    </SafeAreaView>
  );
};
