import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { SafeAreaView } from 'react-native';
import {
  Text,
  Box,
  Spacing,
  WelcomeNestIllustration,
  Button,
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const WelcomeNestScreen: React.FC<any> = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flexDirection="row" justifyContent={'center'} top={10}>
        <Text
          fontFamily={'Montserrat'}
          type={'title3'}
          textAlign={'center'}
          bold
        >
          Тавтай морил
        </Text>
      </Box>

      <Box flex={1} justifyContent={'center'} alignItems={'center'}>
        <Spacing ph={4} pv={40}>
          <Box
            position={'relative'}
            justifyContent={'center'}
            alignItems={'center'}
          >
            <WelcomeNestIllustration />
          </Box>
        </Spacing>
      </Box>
      <Spacing mb={8}>
        <Spacing pr={3} pl={3} pt={3}>
          <Button
            onPress={() => {
              navigation.navigate(NavigationRoutes.LogIn);
            }}
            width="100%"
          >
            <Box
              position={'relative'}
              justifyContent={'center'}
              alignItems={'center'}
            >
            
              <Text
                fontFamily={'Montserrat'}
                role={'white'}
                type={'body'}
                textAlign={'center'}
                bold
              >
                НЭВТРЭХ
              </Text>
            </Box>
          </Button>
        </Spacing>

        <Spacing pr={3} pl={3} pt={3}>
          <Button
            onPress={() => {
              navigation.navigate(NavigationRoutes.SignUp);
            }}
            category="ghost"
            type="primary"
            size="l"
            width="100%"
          >
            <Text
              fontFamily={'Montserrat'}
              role={'primary500'}
              type={'body'}
              textAlign={'center'}
              bold
            >
              Бүртгүүлэх
            </Text>
          </Button>
        </Spacing>
      </Spacing>
    </SafeAreaView>
  );
};
