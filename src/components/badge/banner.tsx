import React from 'react';
import {
  BannerCriticalIcon,
  BannerSuccessIcon,
  BannerWarningIcon,
  BannerInfoIcon,
} from '../icons/bannerIcons';
import { Text, Box, Spacing, Stack } from '../../components';
import { Border, Shadow } from '../core';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';

const mappingToShadows: any = {
  warning: 'caution400',
  info: 'primary500',
  critical: 'destructive400',
  success: 'success400',
};
const mappingToBackgroundColors: any = {
  warning: 'caution200',
  info: 'primary100',
  critical: 'destructive200',
  success: 'success200',
};
const mappingToIconBackground: any = {
  warning: 'caution300',
  info: 'primary200',
  critical: 'destructive300',
  success: 'success300',
};

export const Banner: React.FC<BannerType> = ({
  type = 'info',
  title = 'Мэдэгдэл',
  children,
  // notice,
}) => {
  console.log(type, title, children);
  return (
    <Box flex={1} height={'auto'}>
      <Spacing grow={1} mh={0.5}>
        <Shadow
          grow={1}
          h={2}
          w={0}
          radius={2}
          opacity={0.1}
          role={'primary500'}
        >
          <Shadow
            grow={1}
            h={0}
            w={0}
            radius={2}
            opacity={0.2}
            role={'primary500'}
          >
            <Shadow
              grow={1}
              h={-6}
              w={0}
              radius={0}
              opacity={1}
              role={mappingToShadows[type]}
            >
              <Border grow={1} radius={4}>
                <Box
                  flex={1}
                  height={'auto'}
                  role={mappingToBackgroundColors[type]}
                >
                  <Spacing ph={4} pv={2}>
                    <Box flexDirection={'row'}>
                      <Box width={32} height={32}>
                        <Border radius={16}>
                          <Box
                            width={32}
                            height={32}
                            role={mappingToIconBackground[type]}
                            justifyContent={'center'}
                            alignItems={'center'}
                          >
                            {type == 'info' && (
                              <BannerInfoIcon></BannerInfoIcon>
                            )}
                            {type == 'warning' && (
                              <BannerWarningIcon></BannerWarningIcon>
                            )}
                            {type == 'critical' && (
                              <BannerCriticalIcon></BannerCriticalIcon>
                            )}
                            {type == 'success' && (
                              <BannerSuccessIcon></BannerSuccessIcon>
                            )}
                          </Box>
                        </Border>
                      </Box>
                      <Box flex={1}>
                        <Spacing ml={2}>
                          <Stack size={1}>
                            <Box
                              width={'100%'}
                              height={32}
                              justifyContent={'center'}
                            >
                              <Text bold type={'headline'}>
                                {title}
                              </Text>
                            </Box>
                            <Box>{children}</Box>
                          </Stack>
                        </Spacing>
                      </Box>
                    </Box>
                  </Spacing>
                </Box>
              </Border>
            </Shadow>
          </Shadow>
        </Shadow>
      </Spacing>
    </Box>
  );
};

type BannerType = {
  type: 'warning' | 'info' | 'critical' | 'success';
  title: string;
  children: any;
};
