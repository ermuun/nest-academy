import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const SuccessIconBold: React.FC<IconType> = ({
  width = 28,
  height = 28,
  role = '#4FB83D',
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 28 28" fill="none">
      <Path
        d="M14.0003 27.3334C6.63633 27.3334 0.666992 21.364 0.666992 14C0.666992 6.63602 6.63633 0.666687 14.0003 0.666687C21.3643 0.666687 27.3337 6.63602 27.3337 14C27.3337 21.364 21.3643 27.3334 14.0003 27.3334ZM12.671 19.3334L22.0977 9.90535L20.2123 8.02002L12.671 15.5627L8.89899 11.7907L7.01366 13.676L12.671 19.3334Z"
        fill={role}
      />
    </Svg>
  );
};
