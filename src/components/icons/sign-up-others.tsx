import React from 'react'
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

interface Props {
    width?: string | number;
    height?: string | number;
}

export const SignUpOthers: React.FC<IconType> = ({
    width = 56,
    height = 56
}) => {
    return (
        <Svg width={width} height={height} viewBox="0 0 56 56" fill="none">
            <Path d="M39.6673 49V44.3333C39.6673 41.858 38.684 39.484 36.9336 37.7337C35.1833 35.9833 32.8093 35 30.334 35H11.6673C9.19196 35 6.81799 35.9833 5.06765 37.7337C3.31732 39.484 2.33398 41.858 2.33398 44.3333V49" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <Path d="M20.9993 25.6667C26.154 25.6667 30.3327 21.488 30.3327 16.3333C30.3327 11.1787 26.154 7 20.9993 7C15.8447 7 11.666 11.1787 11.666 16.3333C11.666 21.488 15.8447 25.6667 20.9993 25.6667Z" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <Path d="M53.666 49.0004V44.3337C53.6645 42.2657 52.9762 40.2568 51.7092 38.6224C50.4422 36.988 48.6683 35.8207 46.666 35.3037" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <Path d="M37.334 7.30371C39.3416 7.81775 41.1211 8.98535 42.3918 10.6224C43.6625 12.2595 44.3523 14.273 44.3523 16.3454C44.3523 18.4178 43.6625 20.4312 42.3918 22.0683C41.1211 23.7054 39.3416 24.873 37.334 25.387" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </Svg>
    );
};


