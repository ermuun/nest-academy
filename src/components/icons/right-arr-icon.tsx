import React from 'react'
import Svg, { Path } from 'react-native-svg'

export const RightArrIcon: React.FC<any> = ({ width, height }) => {

    return (
        <Svg width={width} height={height} viewBox="0 0 8 14" fill="none">
            <Path d="M1 13L7 7L0.999999 1" stroke="#172B4D" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        </Svg>
    )
}

