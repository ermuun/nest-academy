import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const HeaderLeftIcon: React.FC<IconType> = ({
  height = 32,
  width = 32,
}) => {
  return (
    <Svg width={height} height={width} viewBox="0 0 32 32" fill="none">
      <Path
        d="M14.36 16l6.566 6.6-1.876 1.885L10.61 16l8.44-8.485L20.925 9.4 14.36 16z"
        fill="#303030"
      />
    </Svg>
  );
};
