import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
  width?: number | string;
  height?: number | string;
  color?: string;
}

export const TrailingIcon: React.FC<Props> = ({
    width = '25',
    height = '24',
    color = '#FAFAFA',
    ...others
}) => {
    return (
        <Svg width={width} height={height} viewBox="0 0 25 24" fill="none" {...others}>
            <Path d="M12.6727 11.9998L9.84375 9.17184L11.2577 7.75684L15.5007 11.9998L11.2577 16.2428L9.84375 14.8278L12.6727 11.9998Z" fill={color}/>
        </Svg>
    )
}
