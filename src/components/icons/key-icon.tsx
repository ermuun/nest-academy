import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
  width?: string | number;
  height?: string | number;
  color?: string;
}

export const KeyIcon: React.FC<Props> = ({
  width = 22,
  height = 22,
  color = '#8F9CB3',
  ...others
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 22 22"
      fill="none"
      {...others}
    >
      <Path
        d="M15.583 12.833h-3.979a5.5 5.5 0 110-3.666h9.48v3.666H19.25V16.5h-3.667v-3.667zm-9.166 0a1.833 1.833 0 100-3.666 1.833 1.833 0 000 3.666z"
        fill={color}
      />
    </Svg>
  );
};
