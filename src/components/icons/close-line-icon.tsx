import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const CloseLineIcon: React.FC<IconType> = ({
  height = 15,
  width = 15,
  role = 'primary500',
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 10 11" fill="none">
      <Path
        d="M5.00005 4.43949L8.71255 0.72699L9.77305 1.78749L6.06055 5.49999L9.77305 9.21249L8.71255 10.273L5.00005 6.56049L1.28755 10.273L0.227051 9.21249L3.93955 5.49999L0.227051 1.78749L1.28755 0.72699L5.00005 4.43949Z"
        fill={colors[role]}
      />
    </Svg>
  );
};
