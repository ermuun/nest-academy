import React, { useEffect, useState } from 'react';
import { Box } from './layout/index';
import { Border, Text } from './core/index';
import { ColorType } from '../components/types';

export const CountdownTimer: React.FC<ProgressType> = ({
  countUp = false,
  onFinish,
  role = 'primary500',
  backgroundRole,
  width,
  height = '4',
  from = 0,
}) => {
  const translateToTimer = (seconds = 0) => {
    let minutes = seconds / 60;
    minutes = Math.floor(minutes);
    let hours = minutes / 60;
    if (seconds < 1) {
      hours = 0;
      minutes = 0;
      seconds = 0.1;
    } else {
      hours = Math.floor(hours);
      seconds = seconds % 60;
      minutes = minutes % 60;
    }
    const timer = [Math.round(hours), Math.round(minutes), Math.round(seconds)];
    return timer;
  };

  const translate = (n = 0) => {
    return Math.floor(n / 1000);
  };

  const startedTime = Date.now();
  const [elapsedTime, setElapsedTime] = useState(0);
  const [rTime, setRTime] = useState(from);

  useEffect(() => {
    if (rTime > 0) {
      const interval = setInterval(() => {
        const currentTime = Date.now();
        setElapsedTime(
          elapsedTime + translate(currentTime) - translate(startedTime)
        );
        setRTime(from - elapsedTime);
        // console.log(elapsedTime)
      }, 1000);
      return () => {
        clearInterval(interval);
      };
    } else {
      setElapsedTime(from);
    }
  }, [elapsedTime]);

  return (
    <Box flex={1} role={backgroundRole} height={20}>
      <Text role={role}>
        {
          countUp
            ? (translateToTimer(elapsedTime)[0] < 10 ? '0' : '') +
              translateToTimer(elapsedTime)[0] +
              (translateToTimer(elapsedTime)[1] < 10 ? ':0' : ':') +
              translateToTimer(elapsedTime)[1] +
              (translateToTimer(elapsedTime)[2] < 10 ? ':0' : ':') +
              translateToTimer(elapsedTime)[2]
            : (translateToTimer(rTime)[0] < 10 ? '0' : '') +
              translateToTimer(rTime)[0] +
              (translateToTimer(rTime)[1] < 10 ? ':0' : ':') +
              translateToTimer(rTime)[1] +
              (translateToTimer(rTime)[2] < 10 ? ':0' : ':') +
              translateToTimer(rTime)[2]
          // translateToTimer(rTime)[0] + ':' + translateToTimer(rTime)[1] + ':' + translateToTimer(rTime) [2]
        }
      </Text>
    </Box>
  );
};

type ProgressType = {
  width?: number | string;
  height?: number | string;
  role?: ColorType;
  backgroundRole?: ColorType;
  from?: number;
  timer?: number;
  hour?: boolean;
  minute?: boolean;
  second?: boolean;
  onFinish?: Function;
  countUp?: boolean;
};
