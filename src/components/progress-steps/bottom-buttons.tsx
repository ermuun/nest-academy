import React, { useContext } from 'react';
import { Dimensions, View } from 'react-native';
import { ProgressStepsContext } from './progress-steps';
import { Box, Spacing, Button, Text, TrailingIcon } from '..';
const { width } = Dimensions.get('window');

interface getScreenNameProps {
  array: any;
  index: number;
  jumpTo: 'next' | 'previous';
}

export const BottomButtons: React.FC<any> = () => {
  const {
    childrens,
    currentPageIndex,
    setCurrentPageIndex,
    lastButtonFunction,
  } = useContext(ProgressStepsContext);

  const onClick = async ({ array, index, jumpTo }: getScreenNameProps) => {
    const length = array.length - 1;
    const { onNext, catchFunction } = array[index].props;

    switch (jumpTo) {
      case 'next':
        if (index === length)
          try {
            await lastButtonFunction();
          } catch (error) {
            catchFunction && catchFunction(error);
          }
        else {
          try {
            (await onNext) && onNext();
            setCurrentPageIndex(index + 1);
          } catch (error) {
            catchFunction && catchFunction(error);
          }
        }
        break;

      case 'previous':
        if (index !== 0) setCurrentPageIndex(index - 1);
        break;
    }
  };

  if (currentPageIndex === 0)
    return (
      <Spacing mb={6}>
        <Box width={width * 0.9} role={'white'}>
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'next',
              })
            }
            category={'fill'}
            size={'l'}
            width={'100%'}
          >
            <Box flexDirection={'row'} alignItems={'center'}>
              <Text
                type={'body'}
                fontFamily={'Montserrat'}
                bold
                role={'white'}
                width={'auto'}
              >
                ДАРААХ
              </Text>
              <Spacing ml={2}>
                <View >
                  <TrailingIcon color={'white'} />
                </View>
              </Spacing>
            </Box>
          </Button>
        </Box>
      </Spacing>
    );
  else
    return (
      <Spacing mb={8}>
        <Box
          width={width * 0.9}
          flexDirection={'row'}
          justifyContent={'space-between'}
          role={'white'}
        >
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'previous',
              })
            }
            category={'ghost'}
            size={'l'}
            width={width * 0.425}
          >
            <Text
              type={'body'}
              fontFamily={'Montserrat'}
              bold
              role={'primary500'}
            >
              ӨМНӨХ
            </Text>
          </Button>
          <Button
            onPress={() =>
              onClick({
                array: childrens,
                index: currentPageIndex,
                jumpTo: 'next',
              })
            }
            category={'fill'}
            size={'l'}
            width={width * 0.425}
          >
            <Box flexDirection={'row'} alignItems={'center'}>
              <Text
                type={'body'}
                fontFamily={'Montserrat'}
                bold
                role={'white'}
                width={'auto'}
              >
                ДАРААХ
              </Text>
              <Spacing ml={1}>
                <View >
                  <TrailingIcon color={'white'} />
                </View>
              </Spacing>
            </Box>
          </Button>
        </Box>
      </Spacing>
    );
};
