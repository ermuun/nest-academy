import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/core';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { ScrollView } from 'react-native';
import { Box } from '../layout';
import { StickyTopBar } from './sticky-topbar';

interface Props {
  Header?: any;
  children?: any;
}

const Tab = createMaterialTopTabNavigator();

export const TopBarNavigator: React.FC<Props> = ({ Header, children }) => {
  const navigation = useNavigation();
  const childrens = React.Children.toArray(children);
  const state = childrens.map((cur: any) => cur.props);
  const [currentScreenIndex, setCurrentScreenIndex] = useState<number>(0);

  return (
    <Box>
      {Header ? (
        <ScrollView stickyHeaderIndices={[1]}>
          {Header && <Header />}
          <StickyTopBar
            state={state}
            currentScreenIndex={currentScreenIndex}
            setCurrentScreenIndex={setCurrentScreenIndex}
            navigation={navigation}
          />
          <Tab.Navigator tabBar={(props): any => <Box />} swipeEnabled={false}>
            {childrens.map((cur: any) => {
              const { name, component } = cur.props;

              return (
                <Tab.Screen
                  key={'tabBar' + name}
                  name={name}
                  component={component}
                />
              );
            })}
          </Tab.Navigator>
        </ScrollView>
      ) : (
        <ScrollView stickyHeaderIndices={[0]}>
          {Header && <Header />}
          <StickyTopBar
            state={state}
            currentScreenIndex={currentScreenIndex}
            setCurrentScreenIndex={setCurrentScreenIndex}
            navigation={navigation}
          />
          <Tab.Navigator tabBar={(props): any => <Box />} swipeEnabled={false}>
            {childrens.map((cur: any) => {
              const { name, component } = cur.props;

              return (
                <Tab.Screen
                  key={'tabBar' + name}
                  name={name}
                  component={component}
                />
              );
            })}
          </Tab.Navigator>
        </ScrollView>
      )}
    </Box>
  );
};
