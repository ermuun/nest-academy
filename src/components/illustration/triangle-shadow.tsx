import * as React from 'react';
import Svg, { Mask, Path, G } from 'react-native-svg';

export const TriangleShadow = () => {
  return (
    <Svg width={185} height={100} viewBox="0 0 185 100" fill="none">
      <Mask id="prefix__a" x={1} y={47} width={147} height={152}>
        <Path
          opacity={1}
          d="M9.587 188.694l14.03-131.588 112.409 68.865-126.44 62.723z"
          stroke="#FCC77D"
          strokeWidth={16}
        />
      </Mask>
      <G mask="url(#prefix__a)" stroke="#FCC77D" strokeWidth={2}>
        <Path d="M4-62v373M9-62v373M14-62v373M19-62v373M24-62v373M29-62v373M34-62v373M39-62v373M44-62v373M49-62v373M54-62v373M59-62v373M64-62v373M69-62v373M74-62v373M79-62v373M84-62v373M89-62v373M94-62v373M99-62v373M104-62v373M109-62v373M114-62v373M119-62v373M124-62v373M129-62v373M134-62v373M139-62v373M144-62v373" />
      </G>
    </Svg>
  );
};
