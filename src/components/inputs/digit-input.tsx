import React, { useRef, useContext, useEffect, useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { Box } from '../layout/box';
import { AuthContext } from '../../provider/auth';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
const styles = StyleSheet.create({
  digit: {
    width: 40,
    height: 47,
    backgroundColor: '#F2F2F7',
    borderRadius: 4,
    textAlign: 'center',
    marginHorizontal: 10,
  },
  onFocus: {
    borderWidth: 1,
    borderColor: '#172B4D',
    borderStyle: 'solid',
  },
  hidden: {
    display: 'none'
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }

});

export const DigitInput: React.FC<any> = () => {
  const { code, setcode } = useContext(AuthContext);
  const [focus, setfocus] = useState(Number);
  const [digit, setdigit] = useState()
  let digits = new Array(6).fill(0);
  const input = useRef(null)
  useEffect(() => {
    code.split('').map((pin: any, index: any) => {
      setdigit((digit: any) => ({ ...digit, [`pin${index}`]: pin }))
    })
    setfocus(code.length)

  }, [code,])
  const Manage = (event: any) => {
    if (event.nativeEvent.key == 'Backspace') {
      setdigit({ ...digit, [`pin${code.length - 1}`]: null })
    }
  }
  return (
    <Box
      height={56}
      flexDirection="row"
      justifyContent="center"
      alignItems="center"
    >
      <TextInput
        ref={input}
        keyboardType="numeric"
        maxLength={6}
        onChangeText={(code) => {
          setcode(code);
        }}
        onKeyPress={(event: any) => Manage(event)}
        style={styles.hidden}
      />
      {digits.map((value, index) => (
        <TouchableWithoutFeedback onPress={() => { input.current?.focus() }} style={styles.center}>
          <View style={[styles.digit, styles.center, focus == index ? styles.onFocus : null]}>
            <Text style={styles.center}>
              {digit?.[`pin${index}`]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      ))}
    </Box>
  );
};
