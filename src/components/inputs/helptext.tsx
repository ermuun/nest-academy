import React from 'react';
import { Box, Text, Spacing } from '..';

type ItemStatus = {
  value?: any | null;
  percent?: any;
  width?: number | string;
  height?: number | string;
};

export const HelpText: React.FC<ItemStatus> = ({
  percent,
  value,
  width,
  height,
}) => {
  return (
    <Box width={width} height={height}>
      <Spacing ph={3.5}>
        <Box
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
          width={'100%'}
        >
          <Text type={'footnote'} role="primary400">
            {value}
          </Text>
          {percent !== undefined ? (
            <Text type={'footnote'} role="primary400" width={50}>
              {percent}/100
            </Text>
          ) : (
            <></>
          )}
        </Box>
      </Spacing>
    </Box>
  );
};
