import React, { useEffect, useRef, useState } from 'react';
import { Animated, StyleSheet, TextInput } from 'react-native';
import { Border, Text, Box, Spacing } from '..';
import { HelpText, InputMessage } from '.';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { EyeIcon, KeyIcon } from '../icons';
import { useTheme } from '../theme-provider';

interface Props {
  placeholder: string;
  type?: 'default' | 'password';
  status?: 'default' | 'error' | 'disabled' | 'success';
  LeftIcon?: any;
  value?: string;
  RigthIcon?: any;
  counter?: boolean;
  helperText?: string;
  keyboardType?:
    | 'ascii-capable'
    | 'numbers-and-punctuation'
    | 'url'
    | 'name-phone-pad'
    | 'twitter'
    | 'web-search';
  messageText?: string;
  messageType?: 'default' | 'error' | 'warning' | 'success';
  onSubmitEditing?: Function;
  onChangeText?: Function;
  multiline?: boolean;
  numberOfLines?: number;
}

export const MultilineInput: React.FC<Props> = (props) => {
  const { colors } = useTheme();
  const {
    type,
    placeholder,
    status = 'default',
    LeftIcon,
    RigthIcon,
    value,
    keyboardType,
    onSubmitEditing,
    onChangeText,
    multiline,
    numberOfLines,
  } = props;
  const animationIndex = useRef(new Animated.Value(0)).current;
  const [isInputFocus, setIsInputFocus] = useState(false);
  const [visible, setVisible] = useState(true);
  const translateYLabel = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -12],
  });
  const translateXLabel = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -1 * placeholder.length * 2],
  });
  const translateYInput = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 7],
  });
  const scale = animationIndex.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0.7],
  });

  useEffect(() => {
    if (isInputFocus) {
      Animated.timing(animationIndex, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  }, [isInputFocus]);

  return (
    <Box opacity={status === 'disabled' ? 0.5 : 1} width={343}>
      <Border
        radius={4}
        lineWidth={isInputFocus ? 2 : 1}
        role={
          status === 'error'
            ? 'destructive500'
            : status === 'success'
            ? 'success500'
            : isInputFocus
            ? 'primary500'
            : 'primary400'
        }
      >
        <Box role={'primary100'} width={339}>
          <Spacing ph={4}>
            <Box
              justifyContent={'center'}
              alignItems={'center'}
              flexDirection={'row'}
            >
              {/* left icon */}
              <Spacing mr={type === 'password' || LeftIcon ? 3 : 0}>
                {type === 'password' ? <KeyIcon /> : LeftIcon && <LeftIcon />}
              </Spacing>
              {/* placeholder */}
              <Animated.View
                style={[
                  styles.label,
                  { left: type === 'password' ? 28 : LeftIcon ? 30 : 0 },
                  {
                    transform: [
                      { translateY: translateYLabel },
                      { scale },
                      { translateX: translateXLabel },
                    ],
                  },
                ]}
              >
                <Text
                  type={'headline'}
                  role={
                    status === 'error'
                      ? 'destructive500'
                      : status === 'success'
                      ? 'success500'
                      : 'primary500'
                  }
                >
                  {placeholder}
                </Text>
              </Animated.View>
              {/* text input */}
              <Animated.View
                style={{
                  marginRight: RigthIcon ? 12 : 0,
                  width:
                    type === 'password'
                      ? '86%'
                      : LeftIcon || RigthIcon
                      ? '92%'
                      : '100%',
                  transform: [{ translateY: translateYInput }],
                }}
              >
                <TextInput
                  style={[styles.input, { color: colors['primary500'] }]}
                  onFocus={() => setIsInputFocus(true)}
                  onBlur={() => setIsInputFocus(false)}
                  value={value}
                  keyboardType={keyboardType}
                  autoFocus={value ? true : false}
                  editable={status === 'disabled' ? false : true}
                  secureTextEntry={type === 'password' ? visible : false}
                  onChangeText={(text) => onChangeText && onChangeText(text)}
                  onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
                  multiline
                />
              </Animated.View>
              {/* rigth icon */}
              <TouchableOpacity
                onPress={() => setVisible(!visible)}
                style={{ display: type === 'password' ? 'flex' : 'none' }}
              >
                <EyeIcon />
              </TouchableOpacity>
              {RigthIcon && <RigthIcon />}
            </Box>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    zIndex: 0,
    top: 10,
  },

  input: {
    minHeight: 45,
    zIndex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 17,
    fontWeight: '400',
    letterSpacing: 1,
  },
});
