import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import { CONTENTFUL_SPACE_ID, CONTENTFUL_CDA_TOKA } from '@env';

export const ApolloClientProvider: React.FC<any> = ({ children }) => {
  const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: `https://graphql.contentful.com/content/v1/spaces/${CONTENTFUL_SPACE_ID}`,
    headers: {
      Authorization: `Bearer ${CONTENTFUL_CDA_TOKA}`,
    },
  });

  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};
