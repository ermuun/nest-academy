import { gql } from "@apollo/client";

export const REQUEST_ABOUT = gql`
    query Abouts {
        aboutCollection {
            total
            items {
                heading1
            }
        }
    }
`;


export const REQUEST_NEWS = gql`
    query DemoProjects {
        blogPostCollection(where: {picture_exists: true}) {
            items{
                sys{
                    id
                },
                title,
                publishedDate,
                shortDescription,
                description{
                    json
                }
                picture {
                    title
                    description
                    contentType
                    fileName
                    size
                    url
                    width
                    height
                },
            }
        }
    }
`;
export const REQUEST_LEAP_SCREEN = gql`
query Leap {
    leapAboutCollection {
      items {
        title
        leapAboutAdvantagesText
      }
    },
    leapAdvantageCollection {
      items {
        advantage
      }
    }
    stageOfCourseCollection {
          items {
            title
            description {
                          json
            }
            hours
          }
    },
    courseCollection {
          items {
            title
            enrollment
            courseDuration
            completeProgram
          }
    },
    teachersCollection {
		items {
		  name
		  class
		  position
		}
    }
  }
`

export const REQUEST_HOP_SCREEN = gql`
    query {
        course(id: "3dO1pfChuYDgy6v7TJBxFc") {
            title
            promotionText {
                json
            }
            expectationText {
                json
            }
            teachersCollection {
                items {
                    name
                    class
                    teacherBioCollection {
                    items {
                        bio
                    }
                    }
                    picture {
                    url
                    }
                }
            }
            stagesOfCourseCollection {
                items {
                    title
                    description {
                        json
                    }
                    hours
                }
            }
        }
    }
`
export const REQUEST_EXAM_SCREEN = gql`
    query {
        exam (id: "60DnSlo24HQnUzIaM3hCXu") {
            designCollection {
                total
                items {
                category
                question {
                    json
                }
                questionPicture {
                    url
                }
                answerPicture {
                    url
                }
                correctAnswers {
                    json
                }
                wrongAnswers {
                    json
                }
                point
            }
        }
        mathCollection {
            total
            items {
                category
                question {
                    json
                }
                questionPicture {
                    url
                }
                answerPicture {
                    url
                }
                correctAnswers {
                    json
                }
                wrongAnswers {
                    json
                }
                point
            }
        }
        iqCollection {
            total
            items {
                category
                question {
                    json
                }
                questionPicture {
                    url
                }
                answerPicture {
                    url
                }
                correctAnswers {
                    json
                }
                wrongAnswers {
                    json
                }
                point
            }
        }
        logicCollection {
            total
            items {
                category
                question {
                    json
                }
                questionPicture {
                    url
                }
                answerPicture {
                    url
                }
                correctAnswers {
                    json
                }
                wrongAnswers {
                    json
                }
                point
            }
        }
    }
    }
`
export const REQUEST_EXAM_SCREEN2 = gql`
query {
    exam (id: "60DnSlo24HQnUzIaM3hCXu") {
      mathCollection {
        total
        items {
          category
          question {
            json
          }
          questionPicture {
            url
          }
          answerPicture {
            url
          }
          correctAnswers {
            json
          }
          wrongAnswers {
            json
          }
          point
        }
      }
    }
  }
`
export const Request_FAQ_ANSWER_VIDEOS = gql`
    query Faq_answer_videos {
        frequentlyAskedQuestionsFaqCollection {
            items {
                answerVideosCollection {
                    items {
                        url
                    }
                }
            }
        }
    }
`